# EEE3088F_Project
## Name
EEE3088F Data Sensor Logging System

## Description
This project will be a design of a HAT for sensing and logging data and will be attached to STM32 microcontroller.

## Collaborate with your team
    Uzile Mkhumbuzi - Interface Module
    Siphesihle - Power Module
    Likhona Mbele - Sensor Module

## Installation
- KiCad Files: To use ____.kicad_pro file, KiCad 6.0 must be used
    Download link: https://www.kicad.org/download/
    Help: https://www.kicad.org/help/

## Usage
- You may use, modify and share project files free of charge.

## License


